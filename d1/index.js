// CRUD Operations
/*
- CRUD is an acronym for: Create, Read, Update, and Delete
- Create: The create functions allows users to create a new record in the database
-Read: The read function is similar to search function. It allows users to search and retrieve specific records.
-Update: the update function is used to modify existing records that are on our database.
-Delete: the delete function allows users to remove records from a database that is no longer needed.
*/

// Create: INSERT Documents
/*
	The mongo shell uses JavaScript for its syntax
	-MongoDB deals with objects as it's structure for documents.
	 We can create documents by providing objects into our methods
	-JavaScript syntax:
		object.object.method({object})
*/

//INSERT ONE
/*
- Syntax
	-db.collectionName.insertOne({object});
*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	}
});

// INSERT MANY
/*
- Syntax:
	- db.collectionName.insertMany([ {objectA}, {objectB}])

*/
db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	course: ["Python", "React", "PHP"],
	department: "none"

},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact {
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	courses : ["React", "Laravel", "Sass"]
}

]);

// READ: FIND/RETRIEVE Documents

/*
-The documents will be returned based on their order storage in the collection // depende sa pagkakaayos kay mongoDB and ibibigay ni robo3t ay parehas
*/

//FIND ALL DOCUMENTS
/*
	Syntax:
		- db.collectionName.find();
*/

db.users.find(); // empty will return all the data inside

//FIND USING SINGLE PARAMETER

/* 
- Syntax:
	- db.collectionName.find({ field: value })
*/

db.users.find({ firstName: "Stephen" });

// FIND USING MULTIPLE PARAMETERS

/*
	Syntax:
		-db.collectionName.find({ fieldA: valueA, fieldB: valueB
		})
*/
db.users.find({ lastName: "Armstrong", age: 82 });

//FIND, PRETTY METHOD
/*
	The "pretty" method allows us to be able to view the documents returned by our terminal in a "prettier" format.
	-Syntax:
		db.collectionName.find({ field:value }).pretty();
*/
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();

// UPDATE: EDIT a document

// UPDATE ONE: Updating a single document
/*
		db.collectionName.updateOne( {criteria}, {$set: {field:value}} );
*/

//For our example, let us create a document that we will then update
// 1. insert initial document
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "last@gmail.com"
	},
	course: [],
	department: "none"
});

// 2. Update the documents
db.users.updateOne(
		{ firstName: "Test" },
		{
			$set: 
			{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "87654321",
					email: "bill@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations"
			}
		}

);

db.users.updateMany(
{department: "none"},
{
	$set: { department: "HR" },
}

);

db.user.find().pretty();

//REPLACE ONE
/*
	Replace one replaces the whole document

	-if updateOne updates specific fields, replaceOne replaces the whole document
	//updateOne changes the fields replaceOne replaces all with one
	-if updateOne updates parts, replaceOne replaces the whole document
*/

db.users.replaceOne({
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@rocketmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
});

// DELTE: DELETING Documents
// for our example, let us create a document that we will delete
/*
	- it is good to practice soft deletiong or archiving of our documents instead of deleting them or removing them from the system
*/

db.users.insertOne({
	firstName: "test"
});

//DELETE ONE: Deleting a single document
/*
	- db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({
	firstName: "test"
});
db.users.find({ firstName: "test" }).pretty();

// DELETE MANY: Delete many documents
/*
-Syntax:
	db.collectionName.deleteMany({criteria});
*/

db.users.deleteMany({
	firstName: "Bill"
});
db.users.find({ firstName: "Bill" }).pretty();

// DELETE ALL: Delete all documents
/*
		db.collectionName.deleteMany({})

*/

// db.users.find({ isActive: false })

//ADVANCE QUERIES
/*
	-Retrieving data with complex data structure

*/
// Query an embedded document
// - An embedded document are those types of documents that contain a document inside a document.
db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();

//Query on nested field
db.users.find({
	"contact.email": "janedoe@gmail.com"
}).pretty();

// Querying an Array with exact elements
db.users.find({courses: ["CSS", "JavaScript", "Python"]}).pretty();

//Querying an Array wihtout regards to order
db.users.find({courses: {$all: ["React", "Pytohn"]}).pretty();

// Querying an embedded Array
db.users.insertOne({
	namearr: [
	{
		nameA: "John"
	},
	{
		nameB: "Tamad"
	}
	]
});

db.users.find({
	namearr: 
	{
		nameA: "Juan"
	}
}).pretty();